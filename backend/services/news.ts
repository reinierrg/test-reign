import {GETNEWS} from "../constants/Server";
import request from "../utils/request";

export const newsHNService = async () => {
    return await request(GETNEWS, {
        method: 'GET'
    });
}