import type { NextApiRequest, NextApiResponse } from 'next'
import {connectNews} from "../../models/News";
import {ResponseFuncs} from "../../models/types";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  //capture request method, we type it as a key of ResponseFunc to reduce typing later
  const method: keyof ResponseFuncs = req.method as keyof ResponseFuncs

  console.log("que vuelta", method);

  // Potential Responses
  const handleCase: ResponseFuncs = {
    // RESPONSE FOR GET REQUESTS
    GET: async (req: NextApiRequest, res: NextApiResponse) => {
      try{
        const {News} = await connectNews();
        const activeNews = await News.find({state:'active'}).sort({ 'created_at': -1 });
        res.status(200).json({success:true, data: activeNews})

      } catch(e: any) {
        res.status(500).json({success: false, message: e?.message})
      }
    },
    // RESPONSE POST REQUESTS
    DELETE: async (req: NextApiRequest, res: NextApiResponse) => {
      try{

        const id: string = req.query.id as string
        console.log("estoy por aqui........", id);
        if (id == "") {
          res.status(404).json({success:false, message:"the id is required"})
        }
        const { News } = await connectNews();
        const doc = await News.findById(id);
        console.log(id, doc._id);
        doc.state = 'inactive';
        await doc.save();
        res.status(200).json({success:true})

      } catch(e: any) {
        res.status(404).json({success: false, message: e?.message})
      }
    },
  }

  // Check if there is a response for the particular method, if so invoke it, if not response with an error
  const response = handleCase[method]
  if (response) response(req, res)
  else res.status(400).json({ error: "No Response for This Request" })

}
