import type { NextApiRequest, NextApiResponse } from 'next'
import {newsHNService} from "../../services/news";
import {addNews} from "../../models/News";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {

  const {method} = req;

  switch(method){
    case "GET":
      try{

        const {hits} =  await newsHNService();
        await addNews(hits);

        res.status(200).json({success: true})
      } catch(e: any) {
        res.status(500).json({success: false, message: e?.message})
      }
      break;
  }

}
