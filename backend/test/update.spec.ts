import {describe, expect, test} from '@jest/globals';
import { createMocks, RequestMethod } from 'node-mocks-http';
import type { NextApiRequest, NextApiResponse } from 'next';
import handler from '../pages/api/update';

describe('update api ', () => {

    function mockRequestResponse(method: RequestMethod = 'GET') {
        const {
            req,
            res,
        }: { req: NextApiRequest; res: NextApiResponse } = createMocks({ method });
        req.headers = {
            'Content-Type': 'application/json',
        };
        return { req, res };
    }

    test('should return a successful response from Notehub', async () => {
        const { req, res } = mockRequestResponse();
        await handler(req, res);

        expect(res.statusCode).toBe(200);
        expect(res.getHeaders()).toEqual({ 'content-type': 'application/json' });
    });

});