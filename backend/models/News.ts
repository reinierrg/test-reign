import mongoose from 'mongoose';
import { newsSchema } from './types';

const { DB_NAME, MONGO_URL } = process.env;
const DATABASE_URL = `${MONGO_URL}/${DB_NAME}`;

// connection function
export const connectNews = async () => {
    const conn = await mongoose
        .connect(DATABASE_URL as string)
        .catch(err => console.log(err))
    console.log("Mongoose Connection Established")

    const News = mongoose.models.News || mongoose.model('News', newsSchema);

    return { conn, News }
}

const isNotExistNews = async (modelNews: any, id: string) => {
    let item =  await modelNews.findOne({objectID: id}).exec();
    console.log(id, item?.objectID, !item?.objectID);
    return !item?.objectID;
}

export const addNews = async (hits: any) => {
    const {News, conn} = await connectNews();

    for (const item of hits) {
        let checked = await isNotExistNews(News, item.objectID);
        if (checked) {
            const newsModel = new News({
                created_at: item.created_at,
                title: item.title,
                url: item.url,
                author: item.author,
                story_text: item.story_text,
                comment_text: item.comment_text,
                num_comments: item.num_comments,
                story_id: item.story_id,
                story_title: item.story_title,
                story_url: item.story_url,
                parent_id: item.parent_id,
                created_at_i: item.created_at_i,
                objectID: item.objectID,
                state: "active"
            })

            newsModel.save(function (err: any) {
                if (err) {
                    console.log("error", err)
                }
            });
        }
    }
}