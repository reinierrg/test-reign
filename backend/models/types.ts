import {Schema} from "mongoose";

export interface ResponseFuncs {
    GET?: Function
    POST?: Function
    PUT?: Function
    DELETE?: Function
}
// _id?: number
export interface INews {
    created_at?: string
    title?: string
    url?: string
    author?: string
    story_text?: string
    comment_text?: string
    num_comments?: number
    story_id?: number
    story_title?: string
    story_url?: string
    parent_id?: number
    created_at_i?: number
    state: string
    objectID: string
}

export const newsSchema = new Schema<INews>({
    created_at: String,
    title: String,
    url: String,
    author: String,
    story_text: String,
    comment_text: String,
    num_comments: Number,
    story_id: Number,
    story_title: String,
    story_url: String,
    parent_id: Number,
    created_at_i: Number,
    state: {
        type: String,
        enum: {
            values: ['active', 'inactive']
        }
    },
    objectID: {
        type: String,
        unique: true,
        required: true
    },
});