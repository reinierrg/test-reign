
const parseJSON = (response: { status: number; json: () => any;  statusText: string | undefined;}) => {
    if (response.status === 204 || response.status === 205) {
        return null;
    }

    return response.json();
}

const checkStatus = (response: { status: number; json: () => any; statusText: string | undefined; }) => {
    if (response.status >= 200 && response.status < 300) {
        return response;
    }

    const error = new Error(response.statusText);
    throw error;
}

const request = (url: string, option: {}) => {
    return fetch(url, option)
        .then(checkStatus)
        .then(parseJSON)
}

export default request