import request from '../utils/request';

export async function newsService() {
	return request( '/news');
}

export async function deleteNewsService(id) {
	return request( `/news?id=${id}`,{
		method: 'DELETE',
	});
}

export async function newsUpdateService() {
	return request( '/update');
}