import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import { configureStore, combineReducers } from '@reduxjs/toolkit'
import createSagaMiddleware from '@redux-saga/core';

import reportWebVitals from '../src/reportWebVitals';
import reducerApp from 'src/containers/App/reducers'
import sagaApp from 'src/containers/App/sagas'
import App from './containers/App';
import './styles/index.css';

const sagaMiddleware = createSagaMiddleware();
const rootReducer = combineReducers({ reducerApp }); //in case you have more than one reducer just comma seprate them here
const store = configureStore({
    reducer: rootReducer,
    middleware: [sagaMiddleware]
});

sagaMiddleware.run(sagaApp);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
       <Provider store={store}>
          <App />
       </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
