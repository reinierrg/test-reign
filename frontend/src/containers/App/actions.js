import { createAction } from "@reduxjs/toolkit";

export const NEWS_REQUEST = createAction("NEWS_REQUEST");
export const NEWS_RESPONSE_SUCCESS = createAction("NEWS_RESPONSE_SUCCESS");
export const NEWS_RESPONSE_FAIL = createAction("NEWS_RESPONSE_FAIL");

export const NEWS_DELETE_REQUEST = createAction("NEWS_DELETE_REQUEST");
export const NEWS_DELETE_SUCCESS = createAction("NEWS_DELETE_SUCCESS");
export const NEWS_DELETE_FAIL = createAction("NEWS_DELETE_FAIL");

export const NEWS_UPDATE_REQUEST = createAction("NEWS_UPDATE_REQUEST");