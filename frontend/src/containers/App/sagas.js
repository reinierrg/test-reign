import {put, takeLatest, all, call} from 'redux-saga/effects';

import {
	NEWS_REQUEST,
	NEWS_RESPONSE_SUCCESS,
	NEWS_RESPONSE_FAIL,
	NEWS_DELETE_REQUEST,
	NEWS_DELETE_FAIL, NEWS_DELETE_SUCCESS, NEWS_UPDATE_REQUEST
} from './actions';
import {deleteNewsService, newsService, newsUpdateService} from '../../services/news';

export function* loadNewsRequest () {
	try{
		const { success, data, message } = yield call(newsService);
		if (success) {
			yield put({type: NEWS_RESPONSE_SUCCESS,payload:{news: data}});
		} else {
			yield put({type: NEWS_RESPONSE_FAIL, payload:{error: message}});
		}
	}catch (e) {
		yield put({type: NEWS_RESPONSE_FAIL, payload:{error: "error querying the backend"}});
	}

}

export function* deleteNewsRequest ({payload}) {
	try{
		const {id} = payload
		const { success, message } = yield call(deleteNewsService, id);
		if (success) {
			yield put({type: NEWS_DELETE_SUCCESS, payload:{id}});
		} else {
			yield put({type: NEWS_DELETE_FAIL, payload:{error: message}});
		}
	} catch (e) {
		yield put({type: NEWS_DELETE_FAIL, payload:{error: "bug removed the news"}});
	}
}

export function* updateNewsRequest () {
	try{
		const { success, message } = yield call(newsUpdateService);
		if (success) {
			yield call(loadNewsRequest);
		} else {
			yield put({type: NEWS_DELETE_FAIL, payload:{error: message}});
		}
	} catch (e) {
		yield put({type: NEWS_DELETE_FAIL, payload:{error: "error updating the news"}});
	}
}


export default function* root() {
	yield all([
		takeLatest(NEWS_REQUEST, loadNewsRequest),
		takeLatest(NEWS_DELETE_REQUEST, deleteNewsRequest),
		takeLatest(NEWS_UPDATE_REQUEST, updateNewsRequest),
	]);
}
