import { createReducer } from "@reduxjs/toolkit";

import {
	NEWS_REQUEST,
	NEWS_RESPONSE_SUCCESS,
	NEWS_RESPONSE_FAIL,
	NEWS_DELETE_SUCCESS,
	NEWS_DELETE_FAIL,
} from './actions';

export const initialState = {
	news: [],
	loading: false,
	error: "",
};

const appReducer = createReducer(initialState, (builder) => {
	builder
		.addCase(NEWS_REQUEST, (state, action) => {
			state.loading = true;
		})
		.addCase(NEWS_RESPONSE_SUCCESS, (state, action) => {
			state.news = action.payload.news;
			state.loading = false;
		})
		.addCase(NEWS_RESPONSE_FAIL, (state, action) => {
			state.error = action.payload.error;
			state.loading = false;
		})
		.addCase(NEWS_DELETE_SUCCESS, (state, action) => {
			state.news = state.news.filter((item) => item._id !== action.payload.id);
			state.loading = false;
		})
		.addCase(NEWS_DELETE_FAIL, (state, action) => {
			state.error = action.payload.error;
			state.loading = false;
		})
		.addDefaultCase(() => {});
});

export default appReducer;
