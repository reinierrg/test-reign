import React, {useRef, useEffect, useState, useContext} from 'react';
import {connect, useSelector, useDispatch} from 'react-redux';
import { FaTrashAlt } from 'react-icons/fa';
import {
	NEWS_DELETE_REQUEST,
	NEWS_REQUEST,
	NEWS_UPDATE_REQUEST,
} from './actions';
import './styles.css'
import {proccessDateTime} from "../../utils/dateUtils";

function App() {

	const dispatch = useDispatch();
	const news = useSelector((store) => store.reducerApp.news);

	useEffect(() => {
		dispatch(NEWS_REQUEST());
	},[]);


	const deleteNews = (id) => {
		dispatch(NEWS_DELETE_REQUEST({id}))
	}

	const toUpdateNews = () => {
		dispatch(NEWS_UPDATE_REQUEST());
	}

	const buildTable = ({data}) => {
		return (
			<table className={"table"}>
				<tbody className={"tbody"}>
				{data.map((item, index)=> (
					<tr key={index} className={"trBody"}>
						<td className={"td"}>
							<a className={"App-link"} target={"_blank"} href={`${item["story_url"]}` || `${item["url"]}`}>{item["story_title"] || item["title"]}</a> <span className={'author'}>{ `- ${item["author"]}`}</span>
						</td>
						<td className={"td"}>
							<p>{proccessDateTime(item["created_at"])}</p>
						</td>
						<td className={"td td-delete"}>
							<a className={"news-delete"} onClick={()=> deleteNews(item["_id"])}><FaTrashAlt/></a>
						</td>
					</tr>
				))}
				</tbody>
			</table>
		);
	}

	return (
		<div className="App">
			<div className="App-header">
				<div onClick={toUpdateNews}><h1>{'HN Feed'}</h1></div>
				<p>{'We <3 hacker news!'}</p>
			</div>
			<div className="App-main">
				{buildTable({data: news})}
			</div>
		</div>
	);
}


export default App;
