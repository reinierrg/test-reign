import moment from 'moment';

export const stringDateToDate = (stringDate) => {
    return moment(stringDate).utc().toDate();
}

export const stringDateToHMMA = (stringDate) => {
    return moment(stringDate).utc().format('h:mm A');
}

export const proccessDateTime = (stringDate) => {

    let dateToCheck = stringDateToDate(stringDate);

    const today = new Date();
    const yesterday = new Date(today);

    yesterday.setDate(yesterday.getDate() - 1);

    if (dateToCheck.toDateString() === today.toDateString()) {
        return stringDateToHMMA(stringDate);
    } else if (dateToCheck.toDateString() === yesterday.toDateString()) {
        return 'Yesterday';
    } else {
        let months = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December',
        ];

        if(dateToCheck.getFullYear() === today.getFullYear()) {
            return `${months[dateToCheck.getMonth()]} ${dateToCheck.getDate()}`;
        } else {
            return `${months[dateToCheck.getMonth()]} ${dateToCheck.getDate()} , ${dateToCheck.getFullYear()}`;
        }
    }

};
